/////////////////////////////////////////////////////
//
// DragonFile - A class for handling .CAS tape files
//              for the Dragon computer emulators.
//
// By Roberto Carlos Fernández Gerhardt aka robcfg
//
// Last update: 24/12/2015 14:37
/////////////////////////////////////////////////////
#ifndef __DRAGONFILE_H__
#define __DRAGONFILE_H__

#define BLOCK_NAME      0x00
#define BLOCK_DATA      0x01
#define BLOCK_END       0xFF
#define BLOCK_BAD       0xBA
#define BLOCK_NAME_SIZE 8

// .CAS file format values definition
#define CAS_PILOT_VAL       0x55
#define CAS_BLOCK_START     0x3C

#include <vector>

using namespace std;

class DragonBlock
{
public:
  DragonBlock(void)
  {
    type        = BLOCK_BAD;
    size        = 0;
    crc         = 0;
    calcCRC     = 0;
    offset      = 0;
    pilotCount  = 0;
    memset(name,0,16);
  }

  DragonBlock( const DragonBlock& other )
  {
    CopyFrom( other );
  }

  ~DragonBlock(void)
  {
  }

  void CopyFrom( const DragonBlock& other )
  {
    type = other.type;
    size = other.size;
    crc = other.crc;
    calcCRC = other.calcCRC;
    offset = other.offset;
    pilotCount = other.pilotCount;
    startPos = other.startPos;

    memcpy( data, other.data, 255 );
    memcpy( name, other.name, 16  );
  }

  unsigned char  type;
  unsigned char  size;
  unsigned char  data[255];
  unsigned char  crc;
  unsigned char  calcCRC;
  unsigned int   offset;
  unsigned int   pilotCount;
  char           name[16];
  unsigned int   startPos;
};

class DragonFile
{
public:
  DragonFile(void)
  {
    pData = 0;
    fileLen = 0;
  }
  ~DragonFile(void)
  {
    if( 0 != pData )
    {
      delete pData;
      pData = 0;
    }
    fileLen = 0;
  }

  bool Save(const char* filename)
  {
    FILE* pFile = fopen(filename, "wb");

    if( 0 != pFile )
    {
      unsigned char pilotVal    = CAS_PILOT_VAL;
      unsigned char blockStart  = CAS_BLOCK_START;

      std::vector<DragonBlock>::const_iterator blockIter = blocks.begin();

      for(; blockIter != blocks.end(); ++blockIter)
      {
        // save pilot tones
        for( unsigned int iPilot = 0; iPilot < blockIter->pilotCount; ++iPilot )
        {
          fwrite(&pilotVal,1,1,pFile);
        }

        // Save block start
        fwrite(&blockStart,1,1,pFile);

        // Save block type
        fwrite(&blockIter->type,1,1,pFile);

        // Save size
        fwrite(&blockIter->size,1,1,pFile);

        // Save data
        fwrite(blockIter->data,1,blockIter->size,pFile);

        // Save CRC
        fwrite(&blockIter->crc,1,1,pFile);
      }

      // Add final pilot tones
      fwrite(&pilotVal,1,1,pFile);
      fwrite(&pilotVal,1,1,pFile);

      fclose(pFile);

      return true;
    }

    return false;
  }

  bool Reload()
  {
    return Load( fileName.c_str() );
  }

  bool Load(const vector<unsigned char>& vData )
  {
    if( vData.empty() )
      return false;

    blocks.clear();

    fileLen = vData.size();

    ProcessFile( vData.data() );

    return true;
  }

  bool Load(const char* filename)
  {
    if( 0 != pData )
      delete pData;

    pData = 0;
    blocks.clear();

    FILE* pFile = fopen(filename, "rb");

    if( 0 != pFile )
    {
      // Get file length
      unsigned int readLen = 0;

      fseek(pFile, 0, SEEK_END);
      fileLen = ftell(pFile);
      fseek(pFile, 0, SEEK_SET);

      // Read data
      pData = new unsigned char[fileLen];
      if( 0 == pData )
      {
        fclose(pFile);
        //cout << "Could not allocate " << fileLen << " bytes of memory. Aborting operation." << endl << endl;
        return false;
      }

      readLen = (unsigned int)fread(pData, 1, fileLen, pFile);

      // Close file
      fclose(pFile);

      // Check Read data size
      if( readLen != fileLen )
      {
        //cout << "Could only read " << readLen << " out of " << fileLen << " bytes. The file is corrupt or damaged." << endl;
        //cout << "Do a disk check and make sure the file is not in use." << endl << endl;
        return false;
      }

      ProcessFile(pData);

      // DEBUG:Save data as binary ////////
/*#ifdef _DEBUG
      std::string binFileName = filename;
      binFileName += ".bin";
      FILE* binFile = fopen( binFileName.c_str(), "wb" );
      for( size_t blockN = 0; blockN < blocks.size(); ++blockN )
      {
        if( blocks[blockN].type == 0 )
        {
          // Data may be stored in name block (e.g. Buzzard Bait)
          fwrite( &blocks[blockN].data[15],1,blocks[blockN].size - 15,binFile);
        }
        else
        {
          fwrite( &blocks[blockN].data[0],1,blocks[blockN].size,binFile);
        }
      }
      fclose( binFile );
#endif*/
      /////////////////////////////////////

      fileName = filename;

      return true;
    }

    return false;
  }

  // Computes a block CRC.
  // The CRC is the sum of all the data values in the block
  // plus the block type and block size.
  unsigned char GetBlockCRC( const DragonBlock& block )
  {
    unsigned char retVal = 0;

    for( unsigned int i = 0; i < block.size; ++i )
    {
      retVal += block.data[i];
    }
    retVal += block.type;
    retVal += block.size;

    return retVal;
  }

  int ProcessFile( const unsigned char* pData )
  {
    unsigned int uPointer = 0;
    int badBlockCount = 0;
    int pilotCount = 0;

    while( uPointer < fileLen )
    {
      if( pData[uPointer] == CAS_PILOT_VAL ) // Pilot wave
      {
        ++pilotCount;
        ++uPointer;
      }
      else if( pData[uPointer] == CAS_BLOCK_START ) // Block
      {
        DragonBlock tmpBlock;

        tmpBlock.type       = pData[++uPointer];
        tmpBlock.size       = pData[++uPointer];
        tmpBlock.startPos   = uPointer > 1 ? uPointer - 2 : uPointer;
        memset(&tmpBlock.data[0], 0, 255);
        memcpy(&tmpBlock.data[0], &pData[++uPointer], tmpBlock.size);
        uPointer           += tmpBlock.size;
        tmpBlock.crc        = pData[uPointer++];
        tmpBlock.offset     = uPointer - tmpBlock.size - 5;
        tmpBlock.pilotCount = pilotCount;

        if( tmpBlock.type == BLOCK_NAME )
        {
          memcpy(&tmpBlock.name[0],&tmpBlock.data[0],BLOCK_NAME_SIZE);
          tmpBlock.name[BLOCK_NAME_SIZE] = 0;
        }

        tmpBlock.calcCRC = GetBlockCRC(tmpBlock);

        //cout << "Block " << (unsigned int)blocks.size() << hex << "(offset 0x" << tmpBlock.offset << ",type " << BlockType2Str(tmpBlock.type);
        //cout << ",size " << (unsigned int)tmpBlock.size << ", crc 0x" << (unsigned int)tmpBlock.crc << ")";
        if( tmpBlock.crc != tmpBlock.calcCRC )
        {
          //cout << " Bad CRC!" << " (is " << (unsigned int)calcCRC << ")";
          ++badBlockCount;
        }
        //cout << endl;

        blocks.push_back( tmpBlock );
        pilotCount = 0;
      }
      else // A misplaced value
      {
        //cout << "Unexpected value found. The file is probably corrupted." << endl << endl;
        return -1;
      }
    }

    return badBlockCount;
  }

  std::vector<DragonBlock> blocks;

private:
  unsigned char* pData;
  unsigned int   fileLen;
  std::string    fileName;
};

#endif
