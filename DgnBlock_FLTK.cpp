/////////////////////////////////////////////////////
//
// DgnBlock - A small graphic interface utility for
//            comparing, checking and fixing Dragon
//            cas files.
//
// By Roberto Carlos Fernández Gerhardt aka robcfg
//
/////////////////////////////////////////////////////
#define _CRT_SECURE_NO_WARNINGS
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Sys_Menu_Bar.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Select_Browser.H>
#include <FL/Fl_Input.H>
#include <string>
#include <algorithm>
#include <iostream>
#include "DragonFile.h"
#ifdef WIN32
#include <windows.h>
#endif

#include "scmversion.h"

using namespace std;

bool 	ChooseFilename		( string& fileName, bool bSaveAsDialog, bool bDirectory = false );
void 	RefreshBrowserData	( Fl_Button* pButton );
string 	TrimString			( const string& _str, size_t maxSize );
void	Compare 			();
void 	HexEditorEdit_cb	(Fl_Widget* pWidget,void *);
void 	HexEditorUpdate_cb	(Fl_Widget* pWidget,void *);
void 	HexEditorReset_cb	(Fl_Widget* pWidget,void *);
void 	HexEditorDone_cb	(Fl_Widget* pWidget,void *);
void 	ExportBin_cb    	(Fl_Widget* pWidget,void *);

///////////////////////////////////////
//
// Classes
//
///////////////////////////////////////
class DgnBlock_HexEditorWindow : public Fl_Double_Window
{
public:
	DgnBlock_HexEditorWindow(int W, int H, const char *l=0);
	DgnBlock_HexEditorWindow(int X, int Y, int W, int H, const char *l=0);
	virtual ~DgnBlock_HexEditorWindow() {}

	void SetBlock( DragonBlock* _block );
	void SetButton( Fl_Button* _button ) { button = _button; }
	Fl_Button* GetButton() { return button; }
	void RefreshBlockData();
	void UpdateBlockData();
	void OverwriteBlock();
	void ResetBlock();
	void SetAlternateBkgColor( int _color );

private:
	// Variables
	Fl_Input*	 hexCells[256];
	Fl_Box*		 cellsBorder;
	Fl_Box*		 asciiBoxes[16];
	Fl_Box*		 infoLabel;
	Fl_Button*	 button;
	Fl_Button*	 updateBlock;
	Fl_Button*	 resetBlock;
	Fl_Button*	 doneEdit;
	DragonBlock* block;
	DragonBlock  editBlock;
	int 		 altBkgColor;

	// Functions
	void CreateControls();
	char FormatAscii( unsigned char val );
};

DgnBlock_HexEditorWindow::DgnBlock_HexEditorWindow(int W, int H, const char *l) : Fl_Double_Window(W,H,l), block(NULL), button(NULL), infoLabel(NULL), altBkgColor(FL_BACKGROUND2_COLOR)
{
	CreateControls();
}

DgnBlock_HexEditorWindow::DgnBlock_HexEditorWindow(int X, int Y, int W, int H, const char *l) : Fl_Double_Window(X,Y,W,H,l), block(NULL), button(NULL), infoLabel(NULL), altBkgColor(FL_BACKGROUND2_COLOR)
{
	CreateControls();
}

void DgnBlock_HexEditorWindow::CreateControls()
{
	int startX = 10;
	int startY = 10;

	cellsBorder = new Fl_Box( FL_BORDER_BOX, startX-1, startY-1, (16*25)+2+20, (16*20)+2, 0 );
	cellsBorder->color( FL_BACKGROUND2_COLOR );

	// ASCII boxes
	for( int iBox = 0; iBox < 16; ++iBox )
	{
		asciiBoxes[iBox] = new Fl_Box( FL_FLAT_BOX, startX+400, startY+(iBox*20), (16*8)+8, 20, " ................ " );
		asciiBoxes[iBox]->align( FL_ALIGN_INSIDE | FL_ALIGN_LEFT );
		asciiBoxes[iBox]->labelfont(FL_COURIER);
		asciiBoxes[iBox]->color( FL_BACKGROUND2_COLOR );
	}

	// Adjust window and controls size
	int labelW = 0;
	int labelH = 0;

	asciiBoxes[0]->measure_label( labelW, labelH );

	int newW = 20 + (16*25) + 10 + labelW;

	resize( x(), y(), 20+(16*25)+labelW, h() );
	cellsBorder->resize( cellsBorder->x(), cellsBorder->y(), (16*25)+2+labelW, cellsBorder->h() );
	for( int iBox = 0; iBox < 16; ++iBox )
	{
		asciiBoxes[iBox]->resize(asciiBoxes[iBox]->x(),asciiBoxes[iBox]->y(),labelW,asciiBoxes[iBox]->h());
	}

	// Input boxes
	for( int y = 0; y < 16; ++y )
	{
		int cellY = startY + (y*20);

		for( int x = 0; x < 16; ++x )
		{
			int cellX = startX + (x*25);

			Fl_Input* cell = new Fl_Input( cellX, cellY, 25, 20 );
			cell->box(FL_FLAT_BOX);
			cell->textfont(FL_COURIER);
			cell->value("FF");
			cell->callback(HexEditorEdit_cb);
			cell->when(FL_WHEN_CHANGED | FL_WHEN_RELEASE);

			hexCells[(y*16)+x] = cell;
		}	
	}

	hexCells[255]->value("FF");

	// Info label
	infoLabel = new Fl_Box( FL_NO_BOX, startX, cellsBorder->y() + cellsBorder->h() + 10, cellsBorder->w(), 30, 0 );
	infoLabel->align( FL_ALIGN_LEFT | FL_ALIGN_INSIDE );

	// Buttons
	int butX = startX;
	int butY = infoLabel->y() + infoLabel->h() + 10;
	int butW = (infoLabel->w() - 20) / 3;
	updateBlock = new Fl_Button( butX, butY, butW, 30, "Update block"); butX += butW + 10;
	resetBlock 	= new Fl_Button( butX, butY, butW, 30, "Reset block");	butX += butW + 10;
	doneEdit 	= new Fl_Button( butX, butY, butW, 30, "Done");

	updateBlock->callback( HexEditorUpdate_cb );
	resetBlock->callback ( HexEditorReset_cb  );
	doneEdit->callback   ( HexEditorDone_cb   );
}

void DgnBlock_HexEditorWindow::SetBlock( DragonBlock* _block )
{
	// Assign block
	block = _block;

	if( NULL == block )
		return;

	ResetBlock();

	// Refresh block data
	RefreshBlockData();
}

void DgnBlock_HexEditorWindow::RefreshBlockData()
{
	if( NULL == block )
		return;

	string tmpStr;
	char hexValStr[3];
	Fl_Widget* focusWidget = Fl::focus();

	for( int y = 0; y < 16; ++y )
	{
		for( int x = 0; x < 16; ++x )
		{
			int dataIdx = (y * 16) + x;

			if( dataIdx < editBlock.size )
			{
				sprintf( hexValStr, (focusWidget == (Fl_Widget*)hexCells[dataIdx]) ? "%X" : "%.2X", editBlock.data[dataIdx] );
				hexCells[dataIdx]->value(hexValStr);
				tmpStr += FormatAscii( editBlock.data[dataIdx] );
			}
			else if( dataIdx == editBlock.size )
			{
				sprintf( hexValStr, (focusWidget == (Fl_Widget*)hexCells[dataIdx]) ? "%X" : "%.2X", editBlock.crc );
				hexCells[dataIdx]->value(hexValStr);
				tmpStr += FormatAscii( editBlock.data[dataIdx] );
			}
			else
			{
				hexCells[dataIdx]->value("");
				tmpStr += " ";
			}
		}

		asciiBoxes[y]->copy_label( tmpStr.c_str() );
		tmpStr = "";
	}

	char buf[256];
	if( editBlock.type == BLOCK_NAME )
		sprintf( buf, "Name: %s Size:%i Load: 0x%.2X%.2X Exec: 0x%.2X%.2X", editBlock.name, (int)editBlock.size, editBlock.data[13], editBlock.data[14], editBlock.data[11], editBlock.data[12] );
	else
		sprintf( buf, "Size:%i", (int)editBlock.size );

	infoLabel->copy_label( buf );
}

char DgnBlock_HexEditorWindow::FormatAscii( unsigned char val )
{
	if( val >= 32 && val < 127 && val != 64 ) // Is 64 a control code?
		return (char)val;
	else
		return '.';
}

void DgnBlock_HexEditorWindow::UpdateBlockData()
{
	if( NULL == block )
		return;

	int row = 0;
	int col = 0;

	for( int idx = 0; idx < editBlock.size; ++idx )
	{
		editBlock.data[idx] = (unsigned char)strtoul( hexCells[idx]->value(), NULL, 16 );
	}

	// Update CRC
	DragonFile dgnFile;
	editBlock.crc = dgnFile.GetBlockCRC( editBlock );
	editBlock.calcCRC = editBlock.crc;
	if( editBlock.type == BLOCK_NAME )
	{
		memcpy(&editBlock.name[0],&editBlock.data[0],BLOCK_NAME_SIZE);
		editBlock.name[BLOCK_NAME_SIZE] = 0;
	}
}

void DgnBlock_HexEditorWindow::OverwriteBlock()
{
	block->CopyFrom( editBlock );
}

void DgnBlock_HexEditorWindow::ResetBlock()
{
	editBlock.CopyFrom( *block );
}

void DgnBlock_HexEditorWindow::SetAlternateBkgColor( int _color )
{ 
	altBkgColor = _color;

	for( int iLine = 1; iLine < 16; iLine += 2 )
	{
		asciiBoxes[iLine]->color( altBkgColor );

		for( int iCell = 0; iCell < 16; ++iCell )
			hexCells[ (iLine * 16) + iCell ]->color( altBkgColor );
	}
}

///////////////////////////////////////
//
// Variables
//
///////////////////////////////////////
Fl_Double_Window* mainWindow = NULL;
DgnBlock_HexEditorWindow* hexEditWindow = NULL;
const int labels_H		= 30;
const int browsers_W	= 330;
const int browsers_H	= 360;
const int mainButtons_W = 100;
const int mainButtons_H = 30;
const int uiPadding_W	= 10;
const int uiPadding_H	= 10;
const int mainWindow_W	= (browsers_W * 2) + mainButtons_W + (uiPadding_W * 4);
const int mainWindow_H	= 510;
const int sameBkg		= FL_FREE_COLOR;
const int diffBkg		= FL_FREE_COLOR + 1;
const int editBkg		= FL_FREE_COLOR + 2;

#ifdef __APPLE__
Fl_Sys_Menu_Bar* menuBar = NULL;
const int menuBarOffset = 0;
#else
Fl_Menu_Bar* menuBar = NULL;
const int menuBarOffset = 0;
#endif
Fl_Box* fileLabel1 = NULL;
Fl_Box* fileLabel2 = NULL;
Fl_Select_Browser *blockBrowser1 = NULL;
Fl_Select_Browser *blockBrowser2 = NULL;
Fl_Button *copyToLeft = NULL;
Fl_Button *copyToRight = NULL;
Fl_Button *compare = NULL;
Fl_Button *load1 = NULL;
Fl_Button *load2 = NULL;
Fl_Button *reload1 = NULL;
Fl_Button *reload2 = NULL;
Fl_Button *delete1 = NULL;
Fl_Button *delete2 = NULL;
Fl_Button *saveAs1 = NULL;
Fl_Button *saveAs2 = NULL;
Fl_Button *editBlock1 = NULL;
Fl_Button *editBlock2 = NULL;
Fl_Button *exportBin1 = NULL;
Fl_Button *exportBin2 = NULL;

DragonFile dragonFile1;
DragonFile dragonFile2;

string tmpString;

///////////////////////////////////////
//
// Callbacks
//
///////////////////////////////////////
void LoadCas_cb(Fl_Widget* pWidget,void *)
{
	string fileName;
	if( !ChooseFilename(fileName,false) )
		return;

	Fl_Button* pButton = (Fl_Button*)pWidget;
	Fl_Box* pLabel = (pButton == load1) ? fileLabel1 : fileLabel2;
	DragonFile* dragonFile = (pButton == load1) ? &dragonFile1 : &dragonFile2;

	bool bLoadOk = dragonFile->Load( fileName.c_str() );

	tmpString = TrimString(fileName, 40);
	bLoadOk ? pLabel->label(tmpString.c_str()) : pLabel->label("(Empty)");

	RefreshBrowserData( pButton );
}

void ReloadCas_cb(Fl_Widget* pWidget,void *)
{
	Fl_Button* pButton = (Fl_Button*)pWidget;
	Fl_Box* pLabel = (pButton == reload1) ? fileLabel1 : fileLabel2;
	DragonFile* dragonFile = (pButton == reload1) ? &dragonFile1 : &dragonFile2;

	if( !dragonFile->Reload() )
		pLabel->label("(Empty)");

	RefreshBrowserData( pButton );
}

void SaveAsCas_cb(Fl_Widget* pWidget,void *)
{
	string fileName;
	if( !ChooseFilename(fileName,true) )
		return;

	Fl_Button* pButton = (Fl_Button*)pWidget;

	if( pButton == saveAs1 )
		dragonFile1.Save( fileName.c_str() );
	else
		dragonFile2.Save( fileName.c_str() );
}

void DeleteSelected_cb(Fl_Widget* pWidget,void *)
{
	Fl_Button* pButton = (Fl_Button*)pWidget;

	DragonFile* dragonFile = (pButton == delete1) ? &dragonFile1 : &dragonFile2;
	Fl_Select_Browser* srcBrowser = (pButton == delete1) ? blockBrowser1 : blockBrowser2;

	int srcBlock = srcBrowser->value() - 2; // Skip 0 and first line
	if( srcBlock >= 0 )
	{
		vector<DragonBlock>::iterator srcIter = dragonFile->blocks.begin();
		srcIter += srcBlock;

		dragonFile->blocks.erase(srcIter);
	
		RefreshBrowserData( pButton );
	}
}

void CopyBlock_cb(Fl_Widget* pWidget,void *)
{
	Fl_Button* pButton = (Fl_Button*)pWidget;
	bool left2Right = (pButton == copyToRight);
	
	DragonFile* src = left2Right ? &dragonFile1 : &dragonFile2;
	DragonFile* dst = left2Right ? &dragonFile2 : &dragonFile1;

	Fl_Select_Browser* srcBrowser = left2Right ? blockBrowser1 : blockBrowser2;
	Fl_Select_Browser* dstBrowser = left2Right ? blockBrowser2 : blockBrowser1;

	int srcBlock = srcBrowser->value() - 2; // Skip 0 and first line
	int dstBlock = dstBrowser->value() - 2; // Skip 0 and first line
	if( srcBlock >= 0 && dstBlock >= 0 )
	{
		vector<DragonBlock>::iterator srcIter = src->blocks.begin();
		srcIter += srcBlock;

		vector<DragonBlock>::iterator dstIter = dst->blocks.begin();
		dstIter += dstBlock;

		dst->blocks.insert( dstIter, *srcIter );

		RefreshBrowserData( pButton );
	}
}

void Compare_cb(Fl_Widget* pWidget,void *)
{
	Compare();
}

void HexEditBlock_cb(Fl_Widget* pWidget,void *)
{
	Fl_Button* button = (Fl_Button*)pWidget;
	DragonFile* dragonFile = (button == editBlock1) ? &dragonFile1 : &dragonFile2;

	Fl_Select_Browser* browser = (button == editBlock1) ? blockBrowser1 : blockBrowser2;

	if( browser->value() < 2 )
		return;

	hexEditWindow->SetButton( button );
	hexEditWindow->SetBlock( &dragonFile->blocks[browser->value()-2] );
	hexEditWindow->show();
	hexEditWindow->redraw();
}

void HexEditorEdit_cb(Fl_Widget* pWidget,void *)
{
	Fl_Input* input = (Fl_Input*)pWidget;
	bool modified = false;

	string inputVal = input->value();
	transform(inputVal.begin(), inputVal.end(),inputVal.begin(), ::toupper);
	if( 0 != strcmp(input->value(), inputVal.c_str()) )
		modified = true;

	// Check size
	if( inputVal.length() > 2 )
	{
		modified = true;
		inputVal = inputVal.substr(0,2);
	}

	// Check ranges
	for( size_t idx = 0; idx < inputVal.length(); ++idx )
	{
		bool ok = false;

		if( (inputVal[idx] >= '0' && inputVal[idx] <= '9') || (inputVal[idx] >= 'A' && inputVal[idx] <= 'F') )
			ok = true;

		if( !ok )
		{
			modified = true;
			inputVal[idx] = '0';
		}
	}

	if( modified )
	{
		input->value(inputVal.c_str());
		input->redraw();
	}

	// Update block data
	hexEditWindow->UpdateBlockData();
	hexEditWindow->RefreshBlockData();

	RefreshBrowserData( hexEditWindow->GetButton() );
}

void HexEditorUpdate_cb(Fl_Widget* pWidget,void *)
{
	hexEditWindow->UpdateBlockData();
	hexEditWindow->OverwriteBlock();
	hexEditWindow->RefreshBlockData();
	RefreshBrowserData( hexEditWindow->GetButton() );
}

void HexEditorReset_cb(Fl_Widget* pWidget,void *)
{
	hexEditWindow->ResetBlock();
	hexEditWindow->RefreshBlockData();
}

void HexEditorDone_cb(Fl_Widget* pWidget,void *)
{
	hexEditWindow->hide();
}

void ExportBin_cb(Fl_Widget* pWidget,void *)
{
	DragonFile* dragonFile = NULL;
	Fl_Button* pButton = (Fl_Button*)pWidget;

	dragonFile = ( pButton == exportBin1 ) ? &dragonFile1 : &dragonFile2;

	// Choose export folder
	string folderName;
	if( !ChooseFilename( folderName, true, true ) )
		return;

#ifdef WIN32
	if( folderName.back() != '\\' ) folderName += '\\';
#else
	if( folderName.back() != '/' ) folderName += '/';
#endif

	FILE* pOut = NULL;
	string fileName;

	for( auto blk : dragonFile->blocks )
	{
		// Create a new file for every 'file' on tape
		if( blk.type == BLOCK_NAME )
		{
			if( pOut )
				fclose(pOut);

			fileName = folderName;
			fileName += blk.name;

			// Clean spaces at the end of the file name
			while( fileName.back() <= 0x20 )
			{
				fileName.pop_back();
			}

			pOut = fopen( fileName.c_str(), "wb" );

			// NOTES:
			// - Some programs (Buzzard Bait) store data in the NAME block 
			// - It might be useful to store LOAD/EXEC addresses somewhere
			if( blk.size > 15 )
				fwrite( &blk.data[15], 1, blk.size - 15, pOut );

			continue;
		}

		// Write block data
		if( blk.size && pOut )
			fwrite( blk.data, 1, blk.size, pOut );

		// Close current file
		if( blk.type == BLOCK_END )
		{
			if( pOut )
				fclose( pOut );
			pOut = NULL;
		}
	}

	// If, for any reason, there's no END block, close file
	if( pOut )
		fclose( pOut );
}

///////////////////////////////////////
//
// Functions
//
///////////////////////////////////////
bool ChooseFilename( string& fileName, bool bSaveAsDialog, bool bDirectory )
{
	// Create native chooser
	Fl_Native_File_Chooser native;
	if( bDirectory )
	{
		native.title( bSaveAsDialog ? "Save to folder" : "Choose folder" );
		native.type(Fl_Native_File_Chooser::BROWSE_DIRECTORY);
		native.options(Fl_Native_File_Chooser::NEW_FOLDER);
	}
	else
	{
		if( bSaveAsDialog )
		{
			native.title("Save as");
			native.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
			native.options(Fl_Native_File_Chooser::SAVEAS_CONFIRM | Fl_Native_File_Chooser::USE_FILTER_EXT);
		}
		else
		{
			native.title("Select file to open");
			native.type(Fl_Native_File_Chooser::BROWSE_FILE);
		}
		native.filter(	"Dragon CAS files\t*.cas\n");
		native.preset_file(".cas");
	}

	// Show native chooser
	switch ( native.show() )
	{
	case -1: return false; break;	// ERROR
	case  1: return false; break;	// CANCEL
	default: 						// PICKED FILE
		if ( native.filename() )
		{
			fileName = native.filename();
		}
	}

	return true;
}

void RefreshBrowserData	( Fl_Button* pButton )
{
	Fl_Select_Browser* blockBrowser = NULL;
	DragonFile* dragonFile = NULL;

	if( pButton == load1 || pButton == reload1 || pButton == delete1 || pButton == copyToLeft || pButton == editBlock1 )
	{
		blockBrowser = blockBrowser1;
		dragonFile = &dragonFile1;
	}
	else
	{
		blockBrowser = blockBrowser2;
		dragonFile = &dragonFile2;
	}

	char tmpBuf[256] = {0};
	unsigned int blockIdx = 0;
	unsigned int blockColor = FL_BLACK;
	bool blockCrcOk = false;
	string blockInfo;

	blockBrowser->clear();

	blockBrowser->add("@f@.Num Type Size CRCF CRCC\n");

	vector<DragonBlock>::const_iterator blockIter;
	for( blockIter = dragonFile->blocks.begin(); blockIter != dragonFile->blocks.end(); ++blockIter, ++blockIdx )
	{
		blockColor = blockIter->crc == blockIter->calcCRC ? FL_BLACK : FL_RED;
		blockInfo = blockIter->crc == blockIter->calcCRC ? blockIter->name : "CRC Mismatch";

		switch( blockIter->type )
		{
			case BLOCK_NAME:
			{
				for( size_t nameIdx = 0; nameIdx < blockInfo.size(); ++nameIdx )
				{
					if( blockInfo[nameIdx] < 32 || blockInfo[nameIdx] > 127 )
						blockInfo[nameIdx] = 0;
				}
				sprintf( tmpBuf, "@f@C%d@.%.3u NAME  %3u  %3u  %3u %s", blockColor, blockIdx, (unsigned int)blockIter->size, (unsigned int)blockIter->crc, (unsigned int)blockIter->calcCRC, blockInfo.c_str() );
				blockBrowser->add( tmpBuf );
			}
			break;
			case BLOCK_DATA:
			{
				sprintf( tmpBuf, "@f@C%d@.%.3u DATA  %3u  %3u  %3u %s", blockColor, blockIdx, (unsigned int)blockIter->size, (unsigned int)blockIter->crc, (unsigned int)blockIter->calcCRC, blockInfo.c_str() );
				blockBrowser->add( tmpBuf );
			}
			break;
			case BLOCK_END :
			{
				sprintf( tmpBuf, "@f@C%d@.%.3u END   %3u  %3u  %3u %s", blockColor, blockIdx, (unsigned int)blockIter->size, (unsigned int)blockIter->crc, (unsigned int)blockIter->calcCRC, blockInfo.c_str() );
				blockBrowser->add( tmpBuf );
			}
			break;
			default:
			{
				sprintf( tmpBuf, "@f@C%d@.%.3u 0x%.02X  %3u  %3u  %3u %s", blockColor, blockIdx, (unsigned int)blockIter->type, (unsigned int)blockIter->size, (unsigned int)blockIter->crc, (unsigned int)blockIter->calcCRC, blockInfo.c_str() );
				//sprintf( tmpBuf, "@f@C%d@.%.3u BAD! ---- ---- ---- Bad block type", FL_RED, blockIdx );
				blockBrowser->add( tmpBuf );
			}
			break;
		}
	}
}

void Compare()
{
	char tmpBuf[256] = {0};
	unsigned int blockColor = FL_BLACK;
	unsigned int blockBkg = FL_BACKGROUND2_COLOR;
	unsigned int blockSame = Fl_Color(sameBkg);
	unsigned int blockDiff = Fl_Color(diffBkg);
	bool blockCrcOk = false;
	string blockInfo;
	Fl_Select_Browser* 	browsers[2] = { blockBrowser1, blockBrowser2 };
	DragonFile* 		dgnFiles[2] = { &dragonFile1 , &dragonFile2  };

	for( int curBrowser = 0; curBrowser < 2; ++curBrowser )
	{
		int otherBrowser = 1 - curBrowser;
		browsers[curBrowser]->clear();
		browsers[curBrowser]->add("@f@.Num Type Size CRCF CRCC\n");

		size_t blocksNum = dgnFiles[curBrowser]->blocks.size();
		for( size_t curblock = 0; curblock < blocksNum; ++curblock )
		{
			DragonBlock* thisBlock  = &dgnFiles[curBrowser]->blocks[curblock];
			blockColor = (thisBlock->crc == thisBlock->calcCRC) ? FL_BLACK : FL_RED;
			blockInfo = thisBlock->crc == thisBlock->calcCRC ? thisBlock->name : "CRC Mismatch";
			blockBkg = FL_BACKGROUND2_COLOR;
			
			if( curblock < dgnFiles[otherBrowser]->blocks.size() )
			{
				DragonBlock* otherBlock = &dgnFiles[otherBrowser]->blocks[curblock];

            	if( 0 != memcmp(thisBlock->data,otherBlock->data,255 ) )
					blockBkg = blockDiff;
				else
					blockBkg = blockSame;
			}
			
			switch( thisBlock->type )
			{
				case BLOCK_NAME:
				{
					for( size_t nameIdx = 0; nameIdx < blockInfo.size(); ++nameIdx )
					{
						if( blockInfo[nameIdx] < 32 || blockInfo[nameIdx] > 127 )
							blockInfo[nameIdx] = 0;
					}
					sprintf( tmpBuf, "@B%u@f@C%d@.%.3u NAME  %3u  %3u  %3u %s", blockBkg, blockColor, (unsigned int)curblock, (unsigned int)thisBlock->size, (unsigned int)thisBlock->crc, (unsigned int)thisBlock->calcCRC, blockInfo.c_str() );
					browsers[curBrowser]->add( tmpBuf );
				}
				break;
				case BLOCK_DATA:
				{
					sprintf( tmpBuf, "@B%u@f@C%d@.%.3u DATA  %3u  %3u  %3u %s", blockBkg, blockColor, (unsigned int)curblock, (unsigned int)thisBlock->size, (unsigned int)thisBlock->crc, (unsigned int)thisBlock->calcCRC, blockInfo.c_str() );
					browsers[curBrowser]->add( tmpBuf );
				}
				break;
				case BLOCK_END :
				{
					sprintf( tmpBuf, "@B%u@f@C%d@.%.3u END   %3u  %3u  %3u %s", blockBkg, blockColor, (unsigned int)curblock, (unsigned int)thisBlock->size, (unsigned int)thisBlock->crc, (unsigned int)thisBlock->calcCRC, blockInfo.c_str() );
					browsers[curBrowser]->add( tmpBuf );
				}
				break;
				default:
				{
					sprintf( tmpBuf, "@B%u@f@C%d@.%.3u 0x%.02X  %3u  %3u  %3u %s", blockBkg, blockColor, (unsigned int)curblock, (unsigned int)thisBlock->type, (unsigned int)thisBlock->size, (unsigned int)thisBlock->crc, (unsigned int)thisBlock->calcCRC, blockInfo.c_str() );
					//sprintf( tmpBuf, "@f@C%d@.%.3u BAD! ---- ---- ---- Bad block type", FL_RED, blockIdx );
					browsers[curBrowser]->add( tmpBuf );
				}
				break;
			}
		}
	}
}

string TrimString( const string& _str, size_t maxSize )
{
	string retVal = "...";

	if( _str.length() <= maxSize )
		return _str;

	retVal += _str.substr( _str.length() - maxSize + retVal.length() );

	return retVal;
}

void CreateControls( const int startX, const int startY )
{
	// Loaded file names' labels //
	int currX 		= startX;
	int currY 		= startY;
	int lowerUI_Y 	= 0;

    // File 1 Label
    fileLabel1 = new Fl_Box( FL_NO_BOX, currX, currY, browsers_W, labels_H, "(Empty)" );
    fileLabel1->align( FL_ALIGN_LEFT | FL_ALIGN_INSIDE );

    // File 2 Label
    currX += fileLabel1->w() + (uiPadding_W * 2) + mainButtons_W;
    fileLabel2 = new Fl_Box( FL_NO_BOX, currX, currY, browsers_W, labels_H, "(Empty)" );
    fileLabel2->align( FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
    currY += labels_H;

    // Block browsers //
	currX  = startX;
	currY += uiPadding_H;

	// File 1 block browser
	blockBrowser1 = new Fl_Select_Browser( currX, currY, browsers_W, browsers_H );
	blockBrowser1->type(FL_HOLD_BROWSER);

	// File 2 block browser
	currX += blockBrowser1->w() + (uiPadding_W * 2) + mainButtons_W;
	blockBrowser2 = new Fl_Select_Browser( currX, currY, browsers_W, browsers_H );
	blockBrowser2->type(FL_HOLD_BROWSER);
	lowerUI_Y = currY + browsers_H + uiPadding_H;

	// Main buttons //
	currX  = startX + blockBrowser1->w() + uiPadding_W;
	currY += mainButtons_H;

	// Copy block from right panel to the left one
    copyToLeft = new Fl_Button( currX, currY, mainButtons_W, mainButtons_H, "<<<");
    copyToLeft->callback(CopyBlock_cb);

	// Copy block from left panel to the right one
	currY += mainButtons_H + uiPadding_H;
    copyToRight = new Fl_Button( currX, currY, mainButtons_W, mainButtons_H, ">>>");
    copyToRight->callback(CopyBlock_cb);

    // Compare contents of both files
	currY += mainButtons_H + uiPadding_H;
    compare = new Fl_Button( currX, currY, mainButtons_W, mainButtons_H, "Compare");
    compare->callback(Compare_cb);

	// Browsers' buttons aka lower ui //
	currX = startX;
	currY = lowerUI_Y;
	int lowerButtons_W = (browsers_W - (uiPadding_W*2)) / 3;

	// Browser 1 controls
	load1 		= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Load"  		); currX += lowerButtons_W + uiPadding_W;
	reload1		= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Reload"		); currX += lowerButtons_W + uiPadding_W;
	editBlock1	= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Edit block"	); currX  = startX; currY += mainButtons_H + uiPadding_H;
	delete1		= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Delete block"); currX += lowerButtons_W + uiPadding_W;
	saveAs1		= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Save As"		); currX += lowerButtons_W + uiPadding_W;
	exportBin1	= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Export bin"	); currY += mainButtons_H + uiPadding_H;
	load1->callback(LoadCas_cb);
	reload1->callback(ReloadCas_cb);
	editBlock1->callback(HexEditBlock_cb);
	saveAs1->callback(SaveAsCas_cb);
	delete1->callback(DeleteSelected_cb);
	exportBin1->callback(ExportBin_cb);

	currX = startX + browsers_W + mainButtons_W + (uiPadding_W * 2);
	currY = lowerUI_Y;

	// Browser 1 controls
	load2 		= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Load"  		); currX += lowerButtons_W + uiPadding_W;
	reload2		= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Reload"		); currX += lowerButtons_W + uiPadding_W;
	editBlock2	= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Edit block"	); currX  = startX + browsers_W + mainButtons_W + (uiPadding_W * 2); currY += mainButtons_H + uiPadding_H;
	delete2		= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Delete block"); currX += lowerButtons_W + uiPadding_W;
	saveAs2		= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Save As"		); currX += lowerButtons_W + uiPadding_W;
	exportBin2	= new Fl_Button( currX, currY, lowerButtons_W, mainButtons_H, "Export bin"	); currY += mainButtons_H + uiPadding_H;
	load2->callback(LoadCas_cb);
	reload2->callback(ReloadCas_cb);
	editBlock2->callback(HexEditBlock_cb);
	saveAs2->callback(SaveAsCas_cb);
	delete2->callback(DeleteSelected_cb);
	exportBin2->callback(ExportBin_cb);
}

void CheckArguments(int argc, char **argv)
{

}

int main(int argc, char **argv)
{
	// Check arguments
	CheckArguments(argc, argv);

	Fl::visual(FL_RGB);

	// Set background colors
	Fl::set_color( sameBkg , 0xDD, 0xFF, 0xDD );
	Fl::set_color( diffBkg , 0xFF, 0xDD, 0xDD );
	Fl::set_color( editBkg , 0xDD, 0xDD, 0xFF );

	// Create main window
	string windowName = "Dragon Cas Block Editor (";
	windowName += scmVersion;
	windowName += ") by Robcfg";
	mainWindow = new Fl_Double_Window(mainWindow_W, mainWindow_H + menuBarOffset, windowName.c_str());
	/*CreateMenuBar();*/
	CreateControls( 10, menuBarOffset );
	mainWindow->end();

#ifdef WIN32
	mainWindow->icon((char*)LoadIcon(fl_display, MAKEINTRESOURCE(101)));
#endif

	// Create hex editor window.
	hexEditWindow = new DgnBlock_HexEditorWindow(566, 420, "Edit block");
	hexEditWindow->set_modal();
	hexEditWindow->SetAlternateBkgColor( editBkg );

	// Show main window
	mainWindow->show(argc, argv);
	mainWindow->redraw();

	int retVal = Fl::run();

	return retVal;
}