# WELCOME #

DgnBlock_FLTK is a small graphical utility I wrote to edit, compare and fix emulator tape files of the Dragon computer.

This program allows you to load two files, compare them, copy blocks between them, edit raw hex data and save the result.

To make it compatible with a wide range of operating systems, this program is based on the [FLTK GUI Toolkit](http://www.fltk.org/index.php).

![DragonBlock.png](https://bitbucket.org/robcfg/dgnblock_fltk/raw/73594a66eb489a7855880c299cc5d1881b33551e/images/DragonBlock.png)

### Compiling the program ###

## OS X and Linux ##
   * Make sure you have g++ installed (either as part of xcode or using **sudo apt-get install g++**).
   * Download [FLTK 1.3.3 source code](http://www.fltk.org/software.php?VERSION=1.3.3).
   * Extract the source code, open a terminal window and go to the path where you extracted the code.
   * Type **./configure** , **make** and **sudo make install** to compile and install the FLTK libraries.
   * Download and extract, or clone the DgnBlock_FLTK repository.
   * Type **chmod +x build_osx.sh** or **chmod +x build_linux.sh**.
   * Type **./build_osx.sh** or **build_linux.sh** to compile the program.
   * Run the program by typing **./DgnBlock**

## Windows ##
   * Install any version of [Microsoft Visual Studio](https://www.visualstudio.com).
   * Download [FLTK 1.3.3 source code](http://www.fltk.org/software.php?VERSION=1.3.3).
   * Extract the source code and open FLTK's Visual studio solution and compile it.
   * Create a new empty Win32 project, and add DgnBlock_FLTK.cpp to the solution.
   * Add the folder where you extracted FLTK's source code to the Additional Include Directories list.
   * Add the lib folder of your FLTK folder to the Additional Library Directories list.
   * Add fltk.lib to the list of libraries to be included.
   * Build the solution.
   * Run the resulting exe file.

Pre-built binaries can be found in the **Downloads** section.