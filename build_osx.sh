#!/bin/bash
export FLTKCONFIG=/usr/local/bin/fltk-config
echo "const char* scmVersion=\"$(git rev-parse --short HEAD)\";" > scmversion.h
g++ DgnBlock_FLTK.cpp `fltk-config --use-forms --use-gl --use-images --ldstaticflags --cxxflags` -std=c++11 -g -o DgnBlock
mv DgnBlock ./DragonBlock.app/Contents/MacOS
